package at.sepp.spaceadventure.draw;

import javafx.scene.image.Image;

public class ImageLoader {
    public static Image ROCKET = new Image("file:assets/rocket.png");
    public static Image ROCKET_HIT = new Image("file:assets/rocket_hit.png");
    public static Image ASTEROID = new Image("file:assets/asteroid.png");
    public static Image ASTEROID_IRIDIUM = new Image("file:assets/asteroid_iridium.png");
    public static Image BULLET = new Image("file:assets/bullet.png");
}
