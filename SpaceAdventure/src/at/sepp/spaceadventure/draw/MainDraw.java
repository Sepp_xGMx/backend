package at.sepp.spaceadventure.draw;

import at.sepp.spaceadventure.clocks.CreateAsteroid;
import at.sepp.spaceadventure.clocks.CreateBullet;
import at.sepp.spaceadventure.entities.Asteroid;
import at.sepp.spaceadventure.entities.Bullet;
import at.sepp.spaceadventure.entities.Player;
import at.sepp.spaceadventure.clocks.StarMovement;
import at.sepp.spaceadventure.game.Gamestate;
import at.sepp.spaceadventure.game.Gamestates;
import at.sepp.spaceadventure.gui.AngledButton;
import at.sepp.spaceadventure.gui.Button;
import at.sepp.spaceadventure.gui.Gui;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.text.DecimalFormat;

public class MainDraw {
    private DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
    private Text text = new Text();

    private int gap = 10;

    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.setFill(Color.BLACK);
        graphicsContext.fillRect(0, 0, Gui.WIDTH, Gui.HEIGHT);

        graphicsContext.setFill(Color.WHITE);

        for (int i = 0; i < StarMovement.STARS.size(); i++)
            graphicsContext.fillOval(StarMovement.STARS.get(i).getX(), StarMovement.STARS.get(i).getY(), StarMovement.STARS.get(i).getSize(), StarMovement.STARS.get(i).getSize());

        if (!Player.HIT) {
            graphicsContext.drawImage(ImageLoader.ROCKET, Player.X - (gap / 2), Player.Y - (gap / 2), Player.WIDTH + gap, Player.HEIGHT + gap);
            //graphicsContext.setStroke(Color.RED);
            //graphicsContext.strokeRect(Player.X + 5, Player.Y + 5, Player.WIDTH - 10, Player.HEIGHT - 10);
        } else {
            graphicsContext.drawImage(ImageLoader.ROCKET_HIT, Player.X - (gap / 2), Player.Y - (gap / 2), Player.WIDTH + gap, Player.HEIGHT + gap);
        }

        for (Asteroid asteroid : CreateAsteroid.ASTEROIDS) {
            if (asteroid.isContainsIridium())
                graphicsContext.drawImage(ImageLoader.ASTEROID_IRIDIUM, asteroid.getX(), asteroid.getY(), asteroid.getWidth(), asteroid.getHeight());
            else
                graphicsContext.drawImage(ImageLoader.ASTEROID, asteroid.getX(), asteroid.getY(), asteroid.getWidth(), asteroid.getHeight());

            if (asteroid.isHit()) {
                graphicsContext.setFill(Color.DARKRED);
                graphicsContext.setStroke(Color.BLACK);
                graphicsContext.fillRect(asteroid.getX() + asteroid.getWidth() / 4, asteroid.getY() - 10, asteroid.getWidth() / 2, 10);

                graphicsContext.setFill(Color.RED);
                graphicsContext.fillRect(asteroid.getX() + asteroid.getWidth() / 4, asteroid.getY() - 10, asteroid.getWidth() / 2 * ((double) asteroid.getCurrentHealth() / (double) asteroid.getBaseHealth()), 10);
            }
            graphicsContext.setStroke(Color.BLUE);
            //graphicsContext.strokeRect(asteroid.getX() + 15, asteroid.getY() + 15, asteroid.getWidth() - 30, asteroid.getHeight() - 30);
        }

        for(Bullet bullet : CreateBullet.BULLETS)
            graphicsContext.drawImage(ImageLoader.BULLET, bullet.getX(), bullet.getY(), bullet.getWidth(), bullet.getHeight());

        if(Gamestate.GAMESTATE == Gamestates.SHOP) {
            graphicsContext.setFill(new Color(0, 0, 0, 0.5));
            graphicsContext.fillRect(0, 0, Gui.WIDTH, Gui.HEIGHT);
            graphicsContext.setStroke(Color.WHITE);
            graphicsContext.strokeLine(Gui.WIDTH / 2, 0, Gui.WIDTH / 2, Gui.HEIGHT);
            graphicsContext.setFill(Color.WHITE);
            graphicsContext.setFont(Font.font("Veranda", 20));

            int tmp;

            for(AngledButton angledButton : Gui.ANGLED_BUTTONS) {
                if(angledButton.isHover()) {
                    graphicsContext.setFill(new Color(1, 1, 1, 0.2));

                    double[] pointsX = {angledButton.getX1(), angledButton.getX2(), angledButton.getX5(), angledButton.getX1()};
                    double[] pointsY = {angledButton.getY1(), angledButton.getY1(), angledButton.getY2(), angledButton.getY2()};
                    graphicsContext.fillPolygon(pointsX, pointsY, 4);

                    double[] pointsX2 = {angledButton.getX3(), angledButton.getX4(), angledButton.getX4(), angledButton.getX6()};
                    double[] pointsY2 = {angledButton.getY1(), angledButton.getY1(), angledButton.getY2(), angledButton.getY2()};
                    graphicsContext.fillPolygon(pointsX2, pointsY2, 4);
                }
            }
        }

        if (Gamestate.GAMESTATE == Gamestates.IDLE) {
            graphicsContext.setFill(new Color(0, 0, 0, 0.5));
            graphicsContext.fillRect(0, 0, Gui.WIDTH, Gui.HEIGHT);

            graphicsContext.setStroke(Color.WHITE);
            graphicsContext.setFill(Color.WHITE);

            for(Button button : Gui.BUTTONS) {
                graphicsContext.strokeRect(button.getX(), button.getY(), button.getWidth(), button.getHeight());
                graphicsContext.setFill(new Color(1, 1, 1, 0.2));

                if(button.isHover())
                    graphicsContext.fillRect(button.getX(), button.getY(), button.getWidth(), button.getHeight());
                graphicsContext.setFill(Color.WHITE);

                text.setText(button.getText());
                text.setFont(Font.font("Veranda", 25));

                graphicsContext.fillText(button.getText(), button.getX() + button.getWidth() / 2 - text.getLayoutBounds().getWidth() / 2, button.getY() + button.getHeight() / 2 + text.getLayoutBounds().getHeight() / 4);
            }
        }
    }
}
