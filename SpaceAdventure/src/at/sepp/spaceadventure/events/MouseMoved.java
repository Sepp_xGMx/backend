package at.sepp.spaceadventure.events;

import at.sepp.spaceadventure.entities.Player;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class MouseMoved implements EventHandler<MouseEvent> {
    @Override
    public void handle(MouseEvent mouseEvent) {
        Player.move((int)mouseEvent.getX());
    }
}
