package at.sepp.spaceadventure.events;

import at.sepp.spaceadventure.game.Gamestate;
import at.sepp.spaceadventure.game.Gamestates;
import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class KeyPressed implements EventHandler<KeyEvent> {
    @Override
    public void handle(KeyEvent keyEvent) {
        switch (keyEvent.getCode()) {
            case ESCAPE:
                if(Gamestate.GAMESTATE == Gamestates.IDLE)
                    Gamestate.GAMESTATE = Gamestates.GAME;
                else
                    Gamestate.GAMESTATE = Gamestates.IDLE;
                break;
        }
    }
}
