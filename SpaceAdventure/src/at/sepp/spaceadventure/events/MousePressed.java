package at.sepp.spaceadventure.events;

import at.sepp.spaceadventure.actions.Collision;
import at.sepp.spaceadventure.entities.Player;
import at.sepp.spaceadventure.game.Gamestate;
import at.sepp.spaceadventure.gui.Gui;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class MousePressed implements EventHandler<MouseEvent> {
    @Override
    public void handle(MouseEvent e) {


        switch (Gamestate.GAMESTATE) {
            case GAME:
                Player.SHOOTING = true;
                break;
            case IDLE:
                if (Collision.cButton(Gui.BUTTONS[0], (int) e.getX(), (int) e.getY())) {
                    Gamestate.state = Gamestate_e.ingame;
                }
                if (Collision.cButton(Gui.BUTTONS[1], (int) e.getX(), (int) e.getY())) {
                    Gamestate.state = Gamestate_e.shop;
                }
                if (Collision.cButton(Gui.BUTTONS[2], (int) e.getX(), (int) e.getY())) {
                    System.exit(0);
                }
                break;
            case SHOP:
                break;
        }

    }
}