package at.sepp.spaceadventure.clocks;

import at.sepp.spaceadventure.entities.Player;

import java.util.Timer;
import java.util.TimerTask;

public class PlayerHitAsteroid {
    public static void start() {
        Timer timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Player.HIT = false;
                timer.cancel();
            }
        }, 300);
    }
}
