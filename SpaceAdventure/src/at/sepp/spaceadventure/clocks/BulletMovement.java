package at.sepp.spaceadventure.clocks;

import at.sepp.spaceadventure.gui.Gui;

public class BulletMovement {

    public static void move() {
        for (int i = 0; i < CreateBullet.BULLETS.size(); i++) {
            CreateBullet.BULLETS.get(i).setY(CreateBullet.BULLETS.get(i).getY() - CreateBullet.BULLETS.get(i).getSpeed());

            if(CreateBullet.BULLETS.get(i).getY() > Gui.HEIGHT)
                CreateBullet.BULLETS.remove(i);
        }
    }
}
