package at.sepp.spaceadventure.clocks;

import at.sepp.spaceadventure.entities.Bullet;
import at.sepp.spaceadventure.entities.Player;
import at.sepp.spaceadventure.game.Gamestate;
import at.sepp.spaceadventure.game.Gamestates;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class CreateBullet {
    public static List<Bullet> BULLETS = new ArrayList<>();
    public static Timer TIMER;

    public CreateBullet() {
        TIMER = new Timer();

        TIMER.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(Gamestate.GAMESTATE == Gamestates.GAME)
                    if(Player.SHOOTING)
                        BULLETS.add(new Bullet(Player.X + Player.WIDTH / 2 - 8, Player.Y - 50, 15, 50, 20));
            }
        }, 200, 200);
    }
}
