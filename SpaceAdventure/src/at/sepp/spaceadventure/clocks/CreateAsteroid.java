package at.sepp.spaceadventure.clocks;

import at.sepp.spaceadventure.entities.Asteroid;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class CreateAsteroid {
    public static List<Asteroid> ASTEROIDS = new ArrayList<>();
    public static Timer TIMER;

    public static int DELAY = 1500;
    public static int PERIOD = 1500;

    public static void start() {
        TIMER = new Timer();

        TIMER.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                ASTEROIDS.add(new Asteroid());
            }
        }, DELAY, PERIOD);
    }
}
