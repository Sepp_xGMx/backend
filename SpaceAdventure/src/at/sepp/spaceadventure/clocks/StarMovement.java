package at.sepp.spaceadventure.clocks;

import at.sepp.spaceadventure.game.Star;
import at.sepp.spaceadventure.gui.Gui;

import java.util.ArrayList;
import java.util.List;

public class StarMovement {

    public static List<Star> STARS = new ArrayList<>();

    public StarMovement() {
        for (int i = 0; i < 100; i++)
            STARS.add(new Star((int) (Math.random() * Gui.WIDTH), (int) (Math.random() * Gui.HEIGHT), (int) (Math.random() * 2) + 1));
    }

    public static void move() {
        for (int i = 0; i < 100; i++) {
            STARS.get(i).setY(STARS.get(i).getY() + 1);

            if (STARS.get(i).getY() >= Gui.HEIGHT)
                STARS.get(i).setY(0);
        }
    }
}
