package at.sepp.spaceadventure.clocks;

import at.sepp.spaceadventure.gui.Gui;

public class AsteroidMovement {
    public static void move() {
        for(int i = 0; i < CreateAsteroid.ASTEROIDS.size(); i++) {
            CreateAsteroid.ASTEROIDS.get(i).setY(CreateAsteroid.ASTEROIDS.get(i).getY() + CreateAsteroid.ASTEROIDS.get(i).getSpeed());

            if(CreateAsteroid.ASTEROIDS.get(i).getY() > Gui.HEIGHT)
                CreateAsteroid.ASTEROIDS.remove(i);
        }
    }
}
