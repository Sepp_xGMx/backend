package at.sepp.spaceadventure.clocks;

import at.sepp.spaceadventure.game.Gamestate;
import at.sepp.spaceadventure.game.Gamestates;
import at.sepp.spaceadventure.gui.Gui;

import java.util.Timer;
import java.util.TimerTask;

public class MainClock {

    private Timer timer;

    public MainClock() {
        timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    Gui.GRAPHICS_CONTEXT.clearRect(0, 0, Gui.WIDTH, Gui.HEIGHT);
                    Gui.DRAW_MAIN.draw(Gui.GRAPHICS_CONTEXT);

                    if (Gamestate.GAMESTATE == Gamestates.GAME) {
                        AsteroidMovement.move();
                        AsteroidCollision.collide();
                        BulletMovement.move();
                        StarMovement.move();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }, 30, 30);
    }
}
