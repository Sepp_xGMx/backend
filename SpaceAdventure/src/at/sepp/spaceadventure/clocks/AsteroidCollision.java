package at.sepp.spaceadventure.clocks;

import at.sepp.spaceadventure.actions.Collision;
import at.sepp.spaceadventure.entities.Player;

public class AsteroidCollision {
    public static void collide() {

        try {
            for (int i = 0; i < CreateAsteroid.ASTEROIDS.size(); i++) {
                for (int j = 0; j < CreateBullet.BULLETS.size(); j++) {
                    if (Collision.bulletCollidesAsteroid(CreateBullet.BULLETS.get(j), CreateAsteroid.ASTEROIDS.get(i))) {
                        CreateAsteroid.ASTEROIDS.get(i).setHit(true);
                        CreateBullet.BULLETS.remove(j);
                        CreateAsteroid.ASTEROIDS.get(i).setCurrentHealth(CreateAsteroid.ASTEROIDS.get(i).getCurrentHealth() - 25);

                        if (CreateAsteroid.ASTEROIDS.get(i).getCurrentHealth() <= 0) {
                            if (CreateAsteroid.ASTEROIDS.get(i).isContainsIridium()) {
                                if (Player.IRIDIUM_COUNT < 9999999999999L)
                                    Player.IRIDIUM_COUNT += CreateAsteroid.ASTEROIDS.get(i).getValue();
                                else
                                    Player.IRIDIUM_COUNT = 9999999999999L;
                            }
                            CreateAsteroid.ASTEROIDS.remove(i);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            for (int i = 0; i < CreateAsteroid.ASTEROIDS.size(); i++) {
                if (Collision.playerCollidesAsteroid(CreateAsteroid.ASTEROIDS.get(i))) {
                    Player.HIT = true;
                    PlayerHitAsteroid.start();
                    CreateAsteroid.ASTEROIDS.remove(i);
                    Player.decrementHealth();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
