package at.sepp.spaceadventure.actions;

import at.sepp.spaceadventure.clocks.CreateAsteroid;
import at.sepp.spaceadventure.clocks.CreateBullet;
import at.sepp.spaceadventure.clocks.MainClock;
import at.sepp.spaceadventure.entities.Player;
import at.sepp.spaceadventure.clocks.StarMovement;
import at.sepp.spaceadventure.gui.Gui;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    private Gui gui = new Gui();

    @Override
    public void start(Stage primaryStage) {
        gui.init();
        gui.create(primaryStage);

        new Player();
        new MainClock();
        CreateAsteroid.start();
        new CreateBullet();
        new StarMovement();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
