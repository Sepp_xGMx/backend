package at.sepp.spaceadventure.actions;

import at.sepp.spaceadventure.entities.Asteroid;
import at.sepp.spaceadventure.entities.Bullet;
import at.sepp.spaceadventure.entities.Player;

public class Collision {
    public static boolean playerCollidesAsteroid(Asteroid asteroid) {
        return (Player.X + 5 < asteroid.getX() + 15 + asteroid.getWidth() - 30 && Player.X + 1 + Player.WIDTH - 10 > asteroid.getX() + 15 && Player.Y + 15 < asteroid.getY() + 15 + asteroid.getHeight() - 30 && Player.Y + 5 + Player.HEIGHT - 10 > asteroid.getY() + 15);
    }

    public static boolean bulletCollidesAsteroid(Bullet bullet, Asteroid asteroid) {
        return (bullet.getX() < asteroid.getX() + asteroid.getWidth() && bullet.getX() + bullet.getWidth() > asteroid.getX() && bullet.getY() < asteroid.getY() + asteroid.getHeight() && bullet.getY() + bullet.getHeight() > asteroid.getY());
    }
}
