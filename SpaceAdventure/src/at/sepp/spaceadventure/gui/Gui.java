package at.sepp.spaceadventure.gui;

import at.sepp.spaceadventure.draw.MainDraw;
import at.sepp.spaceadventure.events.*;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Gui {

    public static MainDraw DRAW_MAIN;

    public static int WIDTH = 1280, HEIGHT = 720;
    public static GraphicsContext GRAPHICS_CONTEXT;

    public static Button[] BUTTONS = new Button[3];
    public static AngledButton[] ANGLED_BUTTONS = new AngledButton[6];

    public static Rectangle[][] RECTANGLES = new Rectangle[ANGLED_BUTTONS.length][5];


    public void init() {
        DRAW_MAIN = new MainDraw();

        BUTTONS[0] = new Button(WIDTH / 2 - 150, HEIGHT / 2 - 125, 300, 50);
        BUTTONS[0].setText("Resume");

        BUTTONS[1] = new Button(WIDTH / 2 - 150, HEIGHT / 2 - 25, 300, 50);
        BUTTONS[1].setText("Shop");

        BUTTONS[2] = new Button(WIDTH / 2 - 150, HEIGHT / 2 + 75, 300, 50);
        BUTTONS[2].setText("Beenden");

        for (int i = 1; i < 4; i++) {
            ANGLED_BUTTONS[i - 1] = new AngledButton(100, i * 150, WIDTH / 3, 50);
            ANGLED_BUTTONS[i + 2] = new AngledButton(100 + WIDTH / 2, i * 150, WIDTH / 3, 50);
        }

        ANGLED_BUTTONS[0].setText("Laser");
        ANGLED_BUTTONS[1].setText("Beschleuniger");
        ANGLED_BUTTONS[2].setText("Schildgenerator");
        ANGLED_BUTTONS[3].setText("Iridiumsammler");
        ANGLED_BUTTONS[4].setText("Iridiumbombe");
        ANGLED_BUTTONS[5].setText("Iridiumschild");

        boolean temp = false;
        int temp2 = 0;
        for (int i = 0; i < RECTANGLES.length; i++) {
            for (int j = 0; j < RECTANGLES[i].length; j++) {
                if (i % 3 == 0 && i != 0) {
                    temp = true;
                    temp2 = 0;
                }

                RECTANGLES[i][j] = new Rectangle(100 + j * 20, 210 + temp2 * 150, 10, 10);

                if (temp) {
                    RECTANGLES[i][j] = new Rectangle(100 + WIDTH / 2 + j * 20, 210 + temp2 * 150, 10, 10);
                }
            }
            temp2 ++;
        }



    }

    public void create(Stage stage) {

        Canvas canvas_main;
        StackPane root = new StackPane();
        int cWtidth = WIDTH - 10, cHeight = HEIGHT - 10;

        canvas_main = new Canvas(WIDTH, HEIGHT);
        GRAPHICS_CONTEXT = canvas_main.getGraphicsContext2D();
        DRAW_MAIN.draw(GRAPHICS_CONTEXT);

        root.getChildren().add(canvas_main);
        Scene scene = new Scene(root, cWtidth, cHeight);

        scene.setOnKeyPressed(new KeyPressed());
        scene.setOnKeyReleased(new KeyReleased());

        scene.setOnMouseMoved(new MouseMoved());
        scene.setOnMouseDragged(new MouseDragged());
        scene.setOnMousePressed(new MousePressed());
        scene.setOnMouseReleased(new MouseReleased());


        stage.setTitle("Space Adventure");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.show();

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Platform.exit();
                System.exit(0);
            }
        });

    }

}
