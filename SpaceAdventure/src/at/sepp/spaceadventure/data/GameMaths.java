package at.sepp.spaceadventure.data;

public class GameMaths {
    public static int limit(int x, int min, int max) {
        return (x > max) ? max : (Math.max(x, min));
    }
}
