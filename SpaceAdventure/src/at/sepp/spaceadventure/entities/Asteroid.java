package at.sepp.spaceadventure.entities;

import at.sepp.spaceadventure.data.GameMaths;
import at.sepp.spaceadventure.gui.Gui;

public class Asteroid {
    private int x;
    private int y;
    private int width;
    private int height;
    private int baseHealth;
    private int currentHealth;
    private int speed;
    private int value = 0;

    private boolean hit = false;
    private boolean containsIridium;

    public Asteroid() {
        int ratio = GameMaths.limit((int)(Math.random() * 200) + 50, 50, 200);
        int random = (int)(Math.random() * 10) + 1;

        this.containsIridium = random % 5 == 0;

        this.width = ratio;
        this.height = ratio;
        this.baseHealth = (this.width * this.height) / 200;
        this.currentHealth = this.baseHealth;
        this.speed = (int)(Math.random() * 6) + 1;
        this.x = GameMaths.limit((int)(Math.random() * Gui.WIDTH), this.width + Player.WIDTH / 2, Gui.WIDTH - this.width - Player.WIDTH / 2);
        this.y = -this.height;

        if(this.containsIridium)
            this.value = (this.width * this.height) / 400;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getBaseHealth() {
        return baseHealth;
    }

    public void setBaseHealth(int baseHealth) {
        this.baseHealth = baseHealth;
    }

    public int getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(int currentHealth) {
        this.currentHealth = currentHealth;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

    public boolean isContainsIridium() {
        return containsIridium;
    }

    public void setContainsIridium(boolean containsIridium) {
        this.containsIridium = containsIridium;
    }
}
