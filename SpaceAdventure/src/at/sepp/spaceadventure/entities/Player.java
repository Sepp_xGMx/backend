package at.sepp.spaceadventure.entities;

import at.sepp.spaceadventure.game.Gamestate;
import at.sepp.spaceadventure.game.Gamestates;
import at.sepp.spaceadventure.gui.Gui;

public class Player {
    public static int X;
    public static int Y;
    public static int WIDTH = 56;
    public static int HEIGHT = 152;
    public static int HEALTH = 3;

    public static long IRIDIUM_COUNT = 1000000L;

    public static boolean SHOOTING = false;
    public static boolean HIT = false;

    public Player() {
        X = Gui.WIDTH / 2 - WIDTH / 2;
        Y = Gui.HEIGHT - 200;
    }

    public static void move(int mouseX) {
        if(Gamestate.GAMESTATE == Gamestates.GAME) {
            if (mouseX >= WIDTH && mouseX <= Gui.WIDTH - WIDTH)
                X = mouseX - WIDTH / 2;
        }
    }

    public static void decrementHealth() {
        if (HEALTH > 1)
            HEALTH--;
        else
            HEALTH = 3;
    }
}
