package com.sap.tsbot.main;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.sap.tsbot.events.Event;

import java.util.logging.Level;

public class Load {
    public static final TS3Config config = new TS3Config();
    public static final TS3Query query = new TS3Query(config);
    public static final TS3Api api = new TS3Api(query);

    public static void main(String[] args) {
        config.setHost("94.250.223.4");
        config.setFloodRate(TS3Query.FloodRate.UNLIMITED);
        config.setDebugLevel(Level.ALL);
        query.connect();
        api.login("Ts Query", "Kprvrk8c");
        api.selectVirtualServerByPort(15056);
        api.setNickname("Sepp's ultimativer Bot");
        Event.loadEvents();
        System.out.println("Bot ist gestartet!");
    }
}
