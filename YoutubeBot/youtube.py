import requests
import bs4
from multiprocessing.pool import ThreadPool as Pool


def get_url(url):
    return requests.get(url)


# response = get_url('http://pyvideo.org/events/pycon-jamaica-2016.html')
response = get_url('http://pyvideo.org/events/pycon-ca-2016.html')


def scrapping(response_value, link_type, tag, search_text):
    soup = bs4.BeautifulSoup(response_value, "html.parser")
    return {links.get(link_type) for links in soup.find_all(tag) if search_text in links.get(link_type)}


pycon_youtube_links = scrapping(response.text, 'href', 'a', 'pycon-ca-2016/')


def youtube_page(youtube_link):
    l = []
    for link in youtube_link:
        r = get_url(link)
        l.append(scrapping(r.text, 'src', 'iframe', 'youtube'))
    return l


youtube = youtube_page(pycon_youtube_links)

''' Prints list of Youtube video links '''
''' Example: [{'https://www.youtube.com/embed/7ProkEKjtL4'}, {'https://www.youtube.com/embed/dEMN67J5-NA'}]'''
# print(youtube)


def video_keys(page):
    return [list(item)[0].split('/')[-1] for item in page]


''' Get a list of all the video Keys '''
keys = video_keys(youtube)

''' Prints list of youtube video IDs '''
''' Example: [\'7ProkEKjtL4\', \'dEMN67J5-NA\'] '''
# print(keys)


def visit_youtube_with(key):
    video_data = {}
    status = get_url('https://www.youtube.com/watch?v=' + key)
    soup = bs4.BeautifulSoup(status.text, "html.parser")
    video_data[status.url] = [soup.select('div#watch-headline-title h1 span.watch-title')[0].get_text().strip(),
                              soup.select('.watch-view-count')[0].get_text()]

    return video_data

final_stats = []
with Pool(5) as p:
    final_stats += p.map(visit_youtube_with, keys)

print("Total number of videos: ", str(len(final_stats)))

for record in final_stats:
    print(record)