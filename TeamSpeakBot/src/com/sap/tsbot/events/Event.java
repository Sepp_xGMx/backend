package com.sap.tsbot.events;

import com.github.theholywaffle.teamspeak3.api.event.*;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import com.sap.tsbot.main.Load;

public class Event {
    public static void loadEvents() {
        Load.api.registerAllEvents();
        Load.api.addTS3Listeners(new TS3Listener() {
            public void onTextMessage(TextMessageEvent textMessageEvent) {

            }

            public void onClientJoin(ClientJoinEvent clientJoinEvent) {
                Client client = Load.api.getClientInfo(clientJoinEvent.getClientId());
                Load.api.sendPrivateMessage(client.getId(), "Willkommen [B]" + client.getNickname() + "[/B]!");
                Load.api.sendPrivateMessage(client.getId(), "Sepp ist der Beste!");
            }

            public void onClientLeave(ClientLeaveEvent clientLeaveEvent) {

            }

            public void onServerEdit(ServerEditedEvent serverEditedEvent) {

            }

            public void onChannelEdit(ChannelEditedEvent channelEditedEvent) {

            }

            public void onChannelDescriptionChanged(ChannelDescriptionEditedEvent channelDescriptionEditedEvent) {

            }

            public void onClientMoved(ClientMovedEvent clientMovedEvent) {

            }

            public void onChannelCreate(ChannelCreateEvent channelCreateEvent) {

            }

            public void onChannelDeleted(ChannelDeletedEvent channelDeletedEvent) {

            }

            public void onChannelMoved(ChannelMovedEvent channelMovedEvent) {

            }

            public void onChannelPasswordChanged(ChannelPasswordChangedEvent channelPasswordChangedEvent) {

            }

            public void onPrivilegeKeyUsed(PrivilegeKeyUsedEvent privilegeKeyUsedEvent) {

            }
        });
    }
}
