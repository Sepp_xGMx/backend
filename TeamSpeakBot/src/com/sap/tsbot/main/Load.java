package com.sap.tsbot.main;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.sap.tsbot.events.Event;

import java.util.logging.Level;

public class Load {

    public static final TS3Config config = new TS3Config();
    public static final TS3Query query = new TS3Query(config);
    public static final TS3Api api = query.getApi();

    public static void main(String[] args) {
        System.out.println("Der SupportBot wurde gestartet!");
        config.setHost("94.250.223.4");
        config.setQueryPort(10011);
        query.connect();
        api.login("T", "h9VXbz+m");
        api.selectVirtualServerById(22704);
        api.setNickname("Bester Bot");
        Event.loadEvents();
    }
}
